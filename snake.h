// Structures definitions
typedef struct Location {
    int x;
    int y;
} Location;

typedef struct Snake {
    Location location[100];
    int length;
    int head;
    int tail;
    char direction;
} Snake;

/*
Create snakes and inserte them into and array of snakes.

Snake *arrayOfSnakes    [Pointer to the array of snakes].
int quantity            [Quantity of snakes to be created].
int length              [Lenght of the body of each snake].
*/
void create_snakes (Snake *arrayOfSnakes, int quantity, int length);

/*
Change the "direction" parameter from a variable type "struct snake".

Search in the keyMaps variable from "keys.c/h" file
for the key pressed and the corresponding player to that key.

Snake *snakes       [Pointer to the array of snakes].
char keyPressed     [The key sended].
*/
void change_snake_direction (Snake *snakes, char keyPressed);

/*
Generate the move of the snake by change tails and heads coordinates,
render the snake on screen and analyze the collisions with borders.

Snake *snake            [Pointer to one snake to be operated].
Snake *arrayOfSnakes    [Pointer to the rest of the snakes].
*/
void render_snake (Snake *snake, Snake *arrayOfSnakes);
