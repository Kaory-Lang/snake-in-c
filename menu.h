/*
    Prepare an array of inputs with its values and return
    this array dynamically allocated in memory.

    Render some layouts and implement movement and editing
    on the layout with [w,a,s,d] keys.
*/
char **menu ();

/*
    char **inputs [An array of inputs getted from **menu()]
    int charc     [Limits of characters by input]
    int argc      [Numbers of unknow arguments passed / inputs] 
    
    Transforms each row of the array of inputs getted by **menu()
    in numbers and assign them into pointers parameters of the
    varaibles that want to assing to.
    
    The pointers arguments (...) are unknown and is required pass
    the number of unknown parameters in (int argc).
*/
void inputs_to_variables (char **inputs, int charc, int argc, ...);

