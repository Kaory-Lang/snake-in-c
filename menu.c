#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>

char menuLayout[] = 
    "╔═══════════════════╗\n"
    "║  SNAKE GAME IN C  ║\n"
    "║      BY KAORY     ║\n"
    "║                   ║\n"
    "║  Width     =      ║\n"
    "║  Height    =      ║\n"
    "║  Players # =      ║\n"
    "║                   ║\n"
    "╚═══════════════════╝\n";

char **menu () {
    int inputsNumber = 3;
    int maxChars = 4;
    int limitX = 16, limitY = 5;
    int cursorX = limitX, cursorY = limitY;
    char key = ' ';
    char character = ' ';

    char *values = calloc(maxChars, sizeof(char));
    char **inputs = calloc(inputsNumber, sizeof(char*));

    for (int row = 0; row < inputsNumber; row++)
        inputs[row] = values + (row * maxChars);

    while (1) {
        system("clear");
        printf(menuLayout);

        if (key == 'q') exit(1);
        
        if (key == 'w' && cursorY != limitY) cursorY--;
        else if (key == 's' && cursorY != limitY+inputsNumber-1) cursorY++;
        else if (key == 'd' && cursorX != limitX+maxChars-1) cursorX++;
        else if (key == 'a' && cursorX != limitX) cursorX--;
        if (key == 0x0D) return inputs;

        char *position = &inputs[cursorY - limitY][cursorX - limitX];
        
        if (isdigit(key)) {
            *position = key;

            if (cursorX != limitX+maxChars-1)
                cursorX++;
            else {
                cursorX = limitX;
                if (cursorY != limitY+inputsNumber-1)
                    cursorY++;
                else 
                    cursorY = limitY;
            }

            key = ' ';
            continue;
        }

        for (int posY = 0; posY < inputsNumber; posY++) {
            for (int posX = 0; posX < maxChars; posX++) {
                char *charValue = &inputs[posY][posX];
                if (*charValue != '\0')
                    printf("\033[%d;%dH%c\n", limitY + posY, limitX + posX, *charValue);
            }
        }

        if (*position == '\0')
            printf("\033[%d;%dH\033[7m \n", cursorY, cursorX);
        else
            printf("\033[%d;%dH\033[7m%c\n", cursorY, cursorX, *position);
        printf("\033[%d;%dH\033[0m\n", cursorY, cursorX + 1);

        system("stty raw -echo");
        key = getchar();
        system("stty -raw echo");
    }
}

void inputs_to_variables (char **inputs, int charc, int argc, ...) {
    va_list argList;
    va_start(argList, argc);
    
    for (int row = 0, result = 0; row < argc; row++) {
        for (int column = 0; column < charc; column++) {
            if (inputs[row][column] != '\0')
                result = (result * 10) + (inputs[row][column] - 0x30);
        }

        int *argPtr = va_arg(argList, int*);
        *argPtr = result;
        result = 0;
    }
}

