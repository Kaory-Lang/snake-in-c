
extern const char keyMaps[5][4][2]; // Mapped keys. MAX 5 players.

/*
return char keyPressed;

Use select() algorithm to check keys pressed
from the input buffer and return the key.

Note: Requires "stty" to prevent the user from
having to press the enter key for each movement
key presse. 
*/
char check_key_pressed ();
