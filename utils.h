// Globals Variables
extern int width;
extern int height;
extern int playersNumber;

extern const char border;
extern const char appleFig;
extern int appleLocation[2];
extern const char snakeBody;

/*
return int randomNumber

Get a random number between two int numbers.
*/
int rangeRand (int lower, int upper);

/*
if int relocate >= 1 the apple will be
relocated on the map.
else just will print the apple without relocate.
*/
void apple_locate (int relocate);
