#include <stdlib.h>
#include <stdio.h>

int width = 0;
int height = 0;
int playersNumber = 0;

const char border = '*';
const char appleFig = '+';
int appleLocation[2];
const char snakeBody = '#';

int rangeRand (int lower, int upper) {
    return (rand() % (upper - lower + 1)) + lower;
}

void apple_locate (int relocate) {
    if (relocate) {
        appleLocation[0] = rangeRand(2, width-1);
        appleLocation[1] = rangeRand(2, height-1);
    }

    printf("\033[%d;%dH%c\n", appleLocation[1], appleLocation[0], appleFig);
}
