#include <stdio.h>
#include <sys/select.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "menu.h"
#include "utils.h"
#include "keys.h"
#include "snake.h"

int fps = 60;
int renderDelay;    // Result in miliseconds

void render_map () {
    for (int line = 1; line <= height; line++) {
        if (line == 1 || line == height) {
            for (int column = 1; column < width; column++) {
                printf("%c", border);
            }
            printf("\n");
        }
        
        printf("\033[%d;0H%c", line, border);
        printf("\033[%d;%dH%c\n", line, width, border);
    }
}

void render_menu () {
    while (width < 21 || height < 9 || playersNumber < 1)
        inputs_to_variables(menu(), 4, 3, &width, &height, &playersNumber);
}

int main (int argc, char *argv[]) {
    srand(time(0));
    renderDelay = (int)((1.0/fps) * 1000);

    render_menu();
    apple_locate(1);

    Snake snakes[playersNumber];
    create_snakes(snakes, playersNumber, 3);

    for (int x = 0; x < 10000; x++) {
        render_map();

        for (int snake = 0; snake < playersNumber; snake++) 
            render_snake(&snakes[snake], &snakes[0]);

        for (int times = 0; times < 10; times++)
            change_snake_direction(snakes, check_key_pressed());

        usleep(renderDelay * 10000);
        system("clear");
    }
    
    system("stty echo");

    return 0;
}
