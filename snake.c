#include "utils.h"
#include "keys.h"
#include <stdlib.h>
#include <stdio.h>

typedef struct Location {
    int x;
    int y;
} Location;

typedef struct Snake {
    Location location[100];
    int length;
    int head;
    int tail;
    char direction;
} Snake;

void create_snakes (Snake *arrayOfSnakes, int quantity, int length) {
    int lower = 1 + length;
    int upperW = width - 1;
    int upperH = (height - 1)/quantity;

    for (int snake = 0; snake < quantity; snake++) {
        int loctX = rangeRand(lower, upperW);
        int loctY = rangeRand(lower, upperH) + (snake * upperH);

        for (int locat = 0; locat < length; locat++) {
            Location ubication = {loctX - locat, loctY};
            arrayOfSnakes[snake].location[locat] = ubication;
        }

        arrayOfSnakes[snake].length = length;
        arrayOfSnakes[snake].head = 0;
        arrayOfSnakes[snake].tail = length - 1;
        arrayOfSnakes[snake].direction = 'R';
    }
}

void change_snake_direction (Snake *snakes, char keyPressed) {
    if (keyPressed == 'q') exit(1);

    for (int keyMap = 0; keyMap < playersNumber; keyMap++) {
        for (int keySet = 0; keySet < 4; keySet++) {
            if (keyMaps[keyMap][keySet][0] == keyPressed)
                snakes[keyMap].direction = keyMaps[keyMap][keySet][1];
        }
    }
}

void render_snake (Snake *snake, Snake *arrayOfSnakes) {
    snake->location[snake->tail] = snake->location[snake->head];
    snake->head = snake->tail;

    if (snake->tail == 0)
        snake->tail = snake->length - 1;
    else
        snake->tail--;

    // Snake redirection
    if (snake->direction == 'R')
        snake->location[snake->head].x++;
    else if (snake->direction == 'L')
        snake->location[snake->head].x--;
    else if (snake->direction == 'U')
        snake->location[snake->head].y--;
    else if (snake->direction == 'D')
        snake->location[snake->head].y++;

    // Snake render
    for (int bodyPos = 0; bodyPos < snake->length; bodyPos++) {
        printf("\033[%d;%dH%c\n",
                snake->location[bodyPos].y,
                snake->location[bodyPos].x,
                snakeBody);
   }

    // Snake Map and Elements Collisions 
    if (snake->location[snake->head].x == width
        || snake->location[snake->head].y == height
        || snake->location[snake->head].x == 1
        || snake->location[snake->head].y == 1) 
            exit(1);
    else if (snake->location[snake->head].x == appleLocation[0]
             && snake->location[snake->head].y == appleLocation[1]) {
                apple_locate(1);
                
                snake->location[snake->length] = snake->location[snake->tail];
                snake->length++;
            }
    else
        apple_locate(0);

    // Body collisions
    for (int cycle = 0; cycle < playersNumber; cycle++) {
        Snake currSnake = arrayOfSnakes[cycle];
        
        for (int bodyPiece = 0; bodyPiece < currSnake.length; bodyPiece++) {
            if (snake->location[snake->head].x == currSnake.location[bodyPiece].x
                && snake->location[snake->head].y == currSnake.location[bodyPiece].y
                && bodyPiece != snake->head) {
                 exit(1);
            }
        }
    }
}

