#include <sys/select.h>
#include <stdlib.h>
#include <stdio.h>

const char keyMaps[5][4][2] = {
    {
        {'w','U'},
        {'a','L'},
        {'s','D'},
        {'d','R'},
    },
    {
        {'t','U'},
        {'f','L'},
        {'g','D'},
        {'h','R'},
    },
    {
        {'i','U'},
        {'j','L'},
        {'k','D'},
        {'l','R'},
    },
    {
        {'[','U'},
        {';','L'},
        {'\'','D'},
        {'\\','R'},
    },
    {
        {'8','U'},
        {'4','L'},
        {'2','D'},
        {'6','R'},
    },
};

char check_key_pressed () {
    fd_set fileDescriptor;
    struct timeval time;
    FD_ZERO(&fileDescriptor);
    FD_SET(0, &fileDescriptor);
    time.tv_sec = 0;
    time.tv_usec = 1;
    int fulfilledEvent;

    system("stty raw -echo");

    fulfilledEvent = select(1, &fileDescriptor, NULL, NULL, &time);

    if (fulfilledEvent == 1) {
        system("stty -raw");
        return getchar();
    }

    system("stty -raw");
    return ' ';
}

